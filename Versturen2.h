/*
 * Versturen.h
 *
 *  Created on: 15 nov. 2021
 *      Author: gijst
 */

#ifndef VERSTUREN_H_
#define VERSTUREN_H_



// voor het versturen naar node-red

void Versturen_Node_red(char* Top, char* PYLD);

typedef struct versturen
{
    char* Top;
    char* PYLD;

    char* rendement;
    char* snelheid;
    char* temp_vis;
    char* belasting;
    char* koppel;
    char* temp_belasting;
    char* vermogen;
    char* verschil_snelheid;
    char* opstelling_gereed;
};


#endif /* VERSTUREN_H_ */
