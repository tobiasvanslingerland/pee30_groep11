/*
 * mqtt_ontvangen.h
 *
 *  Created on: 10 jan. 2022
 *      Author: Tobias
 */

#ifndef MQTT_ONTVANGEN_H_
#define MQTT_ONTVANGEN_H_

int gestart ;
int vermogen_setpoint;
int aandrijving_snelheid;
int handmatig_profiel;
int snelheid_profiel;
int belasting_profiel;
int omgeving_temperatuur;
int aandrijving_stroom;
int aandrijving_spanning;
int belasting_vermogen;
int belasting_max_snelheid;
int belasting_temperatuur;

#endif /* MQTT_ONTVANGEN_H_ */
