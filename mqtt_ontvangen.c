#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <ti/net/mqtt/mqttclient.h>
#include "debug_if.h"
#include "mqtt_ontvangen.h"
#include "versturen2.h"
#include "mqtt_if.h"
extern struct versturen versturen1;
extern MQTTClient_Handle mqttClientHandle;
int x = 0;

void MQTT_start(char* topic, char* payload, uint8_t qos){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    x++;
    if (x == 1){
        versturen1.Top = 14;
    }
    if (x == 2){
        gestart = 1;
    }
}

void MQTT_vermogen_setpoint(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    vermogen_setpoint = atoi(payload);
    }
}

void MQTT_aandrijving_snelheid(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    aandrijving_snelheid = atoi(payload);
    }
    else if(x==2 && handmatig_profiel == 2)
    {
        aandrijving_snelheid = atoi(payload);
    }
}

void MQTT_handmatig_profiel(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);

    handmatig_profiel = atoi(payload);
    }
}

void MQTT_snelheid_profiel(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    snelheid_profiel = atoi(payload);
    }
}

void MQTT_belasting_profiel(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    belasting_profiel = atoi(payload);
    }
}

void MQTT_omgeving_temperatuur(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    omgeving_temperatuur = atoi(payload);
    }
}

void MQTT_aandrijving_stroom(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    aandrijving_stroom = atoi(payload);
    }
}

void MQTT_aandrijving_spanning(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    aandrijving_spanning = atoi(payload);
    }
}

void MQTT_belasting_vermogen(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    belasting_vermogen = atoi(payload);
    }
}

void MQTT_belasting_max_snelheid(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    belasting_max_snelheid = atoi(payload);
    }
}

void MQTT_belasting_temperatuur(char* topic, char* payload, uint8_t qos){
    if (x == 1){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    belasting_temperatuur = atoi(payload);
    }
}

void MQTT_stopknop(char* topic, char* payload, uint8_t qos){
    LOG_INFO("TOPIC: %s PAYLOAD: %s QOS: %d\r\n", topic, payload, qos);
    x = 0;
    gestart = 0;
}

