################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../cc32xxs_tirtos.cmd 

SYSCFG_SRCS += \
../common.syscfg \
../image.syscfg 

C_SRCS += \
../Versturen2.c \
../aandrijving.c \
../belasting.c \
./syscfg/ti_drivers_config.c \
./syscfg/ti_net_config.c \
./syscfg/ti_drivers_net_wifi_config.c \
../main_tirtos.c \
../mqtt_client_app.c \
../mqtt_if.c \
../mqtt_ontvangen.c \
../network_if.c \
../pwmled2.c \
../uart_term.c 

GEN_FILES += \
./syscfg/ti_drivers_config.c \
./syscfg/ti_net_config.c \
./syscfg/ti_drivers_net_wifi_config.c 

GEN_MISC_DIRS += \
./syscfg/ \
./syscfg/ 

C_DEPS += \
./Versturen2.d \
./aandrijving.d \
./belasting.d \
./syscfg/ti_drivers_config.d \
./syscfg/ti_net_config.d \
./syscfg/ti_drivers_net_wifi_config.d \
./main_tirtos.d \
./mqtt_client_app.d \
./mqtt_if.d \
./mqtt_ontvangen.d \
./network_if.d \
./pwmled2.d \
./uart_term.d 

OBJS += \
./Versturen2.obj \
./aandrijving.obj \
./belasting.obj \
./syscfg/ti_drivers_config.obj \
./syscfg/ti_net_config.obj \
./syscfg/ti_drivers_net_wifi_config.obj \
./main_tirtos.obj \
./mqtt_client_app.obj \
./mqtt_if.obj \
./mqtt_ontvangen.obj \
./network_if.obj \
./pwmled2.obj \
./uart_term.obj 

GEN_MISC_FILES += \
./syscfg/ti_drivers_config.h \
./syscfg/ti_utils_build_linker.cmd.genlibs \
./syscfg/syscfg_c.rov.xs \
./syscfg/ti_utils_runtime_model.gv \
./syscfg/ti_utils_runtime_Makefile \
./syscfg/ti_drivers_net_wifi_config.json 

GEN_MISC_DIRS__QUOTED += \
"syscfg\" \
"syscfg\" 

OBJS__QUOTED += \
"Versturen2.obj" \
"aandrijving.obj" \
"belasting.obj" \
"syscfg\ti_drivers_config.obj" \
"syscfg\ti_net_config.obj" \
"syscfg\ti_drivers_net_wifi_config.obj" \
"main_tirtos.obj" \
"mqtt_client_app.obj" \
"mqtt_if.obj" \
"mqtt_ontvangen.obj" \
"network_if.obj" \
"pwmled2.obj" \
"uart_term.obj" 

GEN_MISC_FILES__QUOTED += \
"syscfg\ti_drivers_config.h" \
"syscfg\ti_utils_build_linker.cmd.genlibs" \
"syscfg\syscfg_c.rov.xs" \
"syscfg\ti_utils_runtime_model.gv" \
"syscfg\ti_utils_runtime_Makefile" \
"syscfg\ti_drivers_net_wifi_config.json" 

C_DEPS__QUOTED += \
"Versturen2.d" \
"aandrijving.d" \
"belasting.d" \
"syscfg\ti_drivers_config.d" \
"syscfg\ti_net_config.d" \
"syscfg\ti_drivers_net_wifi_config.d" \
"main_tirtos.d" \
"mqtt_client_app.d" \
"mqtt_if.d" \
"mqtt_ontvangen.d" \
"network_if.d" \
"pwmled2.d" \
"uart_term.d" 

GEN_FILES__QUOTED += \
"syscfg\ti_drivers_config.c" \
"syscfg\ti_net_config.c" \
"syscfg\ti_drivers_net_wifi_config.c" 

C_SRCS__QUOTED += \
"../Versturen2.c" \
"../aandrijving.c" \
"../belasting.c" \
"./syscfg/ti_drivers_config.c" \
"./syscfg/ti_net_config.c" \
"./syscfg/ti_drivers_net_wifi_config.c" \
"../main_tirtos.c" \
"../mqtt_client_app.c" \
"../mqtt_if.c" \
"../mqtt_ontvangen.c" \
"../network_if.c" \
"../pwmled2.c" \
"../uart_term.c" 

SYSCFG_SRCS__QUOTED += \
"../common.syscfg" \
"../image.syscfg" 


