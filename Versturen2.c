
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <ti/net/mqtt/mqttclient.h>
#include "mqtt_if.h"
#include "Versturen2.h"

extern MQTTClient_Handle mqttClientHandle;

struct versturen versturen1;

void Versturen_Node_red(char* Top , char* PYLD) {

    if(Top == 1){
    MQTT_IF_Publish(mqttClientHandle,
                    "pee30/groep_11/temp_vis",
                                               PYLD,
                                               strlen("temp\r\n"),
                                               MQTT_QOS_2);
}
if(Top == 2){
   MQTT_IF_Publish(mqttClientHandle,
                   "pee30/groep_11/snelheid_vis",
                                              PYLD,
                                              strlen("snelheid\r\n"),
                                              MQTT_QOS_2);
}
if(Top == 3 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/belasting_vis",
                                              PYLD,
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
}
if(versturen1.Top == 4){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/overheat",
                                              "overheat",
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
   versturen1.Top = 0;
}
if(versturen1.Top == 5 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/overspeed",
                                              "overspeed",
                                              strlen("snelheid\r\n"),
                                              MQTT_QOS_2);
   versturen1.Top = 0;
}
if(versturen1.Top == 6 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/overload_aandrijving",
                                              "overload",
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
   versturen1.Top = 0;
}

if(Top == 7 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/rendement",
                                              PYLD,
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
}
if(Top == 8 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/koppel",
                                              PYLD,
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
}
if(Top == 9 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/temp_belasting",
                                              PYLD,
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
}

if(versturen1.Top == 10 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/overload_belasting",
                                              PYLD,
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
   versturen1.Top = 0;
}
if(versturen1.Top == 11 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/overvoltage",
                                              PYLD,
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
   versturen1.Top = 0;
}
if(Top == 12 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/rendement_belasting",
                                              PYLD,
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
}
if(Top == 13 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/vermogen",
                                              PYLD,
                                              strlen("belasting\r\n"),
                                              MQTT_QOS_2);
}

if(versturen1.Top == 14 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/opstelling_gereed",
                                              "gereed",
                                              strlen("gereed\r\n"),
                                              MQTT_QOS_2);
   versturen1.Top = 0;
}
if(Top == 15 ){
   MQTT_IF_Publish(mqttClientHandle,
                                               "pee30/groep_11/verschil_snelheid",
                                              PYLD,
                                              strlen("gereed\r\n"),
                                              MQTT_QOS_2);
}
}

