#include <stdint.h>
#include <stddef.h>
#include <unistd.h>
/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/display/Display.h>
/* Driver configuration */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ti_drivers_config.h"
#include "Versturen2.h"
#include "mqtt_ontvangen.h"
#include "aandrijving.h"
#define TASKSTACKSIZE       640
extern struct versturen versturen1;

// vermogen_setpoint, alleen als handmatig is geselecteerd
// aandrijving_snelheid, snelheid setpoint
// handmatig_profiel, 1 of 2, 1 = profiel 2 = handmatig
// snelheid_profiel, 1 of 2, 1 = constante snelheid 2 = oplopende snelheid // vermogen setpoint maximaal
int oplopende_snelheid;
int snelheid_setpoint;
int vermogen_setpoint;
int oplopende_vermogen;
// omgeving_temperatuur, 0 - 25 watt
// aandrijving_stroom, maximale stroom
// aandrijving_spanning, maximale spanning
// belasting_vermogen, maximale vermogen
// belasting_max_snelheid,max snelheid
// belasting_temperatuur, max temp
uint8_t spanning_DUT; //aandrijving
uint8_t stroom_DUT; //aandrijving
int aandrijving_rendement;// aandrijving
int snelheid; //aandrijving
int belasting_rendement; //belasting
int temperatuur; //belasting
int omgeving_temperatuur1;
int koppel; //belasting
int vermogen; //belasting
int status_aandrijving; // 0x01 = Overload, 0x02 = Overvoltage, 0x03 = Noodstop, 0x04 = Gereed, 0x05 = Device actief, 0x06 = niet init.
int status_belasting; // 0x01 = Overload, 0x02 = Overspeed, 0x03 = Overheat, 0x04 = Noodstop, 0x05 = Gereed, 0x06 = Device actief, 0x07 = niet init.
uint8_t         txBuffer[3];
uint8_t         rxBuffer[2];
typedef enum {OVERLOAD_B,OVERSPEED,OVERLOAD_A,OVERHEAT,OVERVOLTAGE}noodstop;
char rendement_buffer[10] = "";
char temperatuur_buffer[10] = "";
char snelheid_buffer[10] = "";
char koppel_buffer[10] = "";
char vermogen_a_buffer[10] = "";
char vermogen_b_buffer[10] = "";
char verschil_snelheid_buffer[10] = "";
char omgeving_temperatuur_buffer[10] = "";
/*
 *  ======== mainThread ========
 */
void verstuur_aandrijving(int adres)
{
        //adres=0x06; //adres aandrijving = 0x06;
        I2C_Params params;
        I2C_Params_init(&params);
        // Open I2C bus for usage
        I2C_Handle i2cHandle = I2C_open(CONFIG_I2C_0, &params);
        I2C_Transaction i2cTransaction;
        i2cTransaction.slaveAddress = adres;
        i2cTransaction.writeBuf   = txBuffer;
        i2cTransaction.writeCount = 3;
        i2cTransaction.readBuf    = rxBuffer;
        i2cTransaction.readCount  = 0;

        I2C_Transaction lezen;
        lezen.slaveAddress = adres;
        lezen.writeBuf   = txBuffer;
        lezen.writeCount = 1;
        lezen.readBuf    = rxBuffer;
        lezen.readCount  = 2;

        txBuffer[0]=0x00; // Status systeem opvragen
        I2C_transfer(i2cHandle, &lezen);
        status_aandrijving = rxBuffer[0];

        if(rxBuffer[0] == 0x06) // aandrijving is niet gereed
        {
        txBuffer[0]=0x05; // start aandrijving register
        txBuffer[1]=0x02; //start init
        txBuffer[2]=0x00;
        I2C_transfer(i2cHandle, &i2cTransaction);
        __delay_cycles(1000);

        txBuffer[0]=0x06; // Maximale stroom register
        txBuffer[1]= aandrijving_stroom;
        txBuffer[2]= aandrijving_stroom<<8;
        I2C_transfer(i2cHandle, &i2cTransaction);
        __delay_cycles(1000);

        txBuffer[0]=0x07; //Maximale spanning register
        txBuffer[1]=aandrijving_spanning;
        txBuffer[2]=aandrijving_spanning<<8;
        I2C_transfer(i2cHandle, &i2cTransaction);
        __delay_cycles(1000);

        txBuffer[0]=0x08; //snelheid  setpoint register
        txBuffer[1]= 0x00;
        txBuffer[2]= 0x00;
        I2C_transfer(i2cHandle, &i2cTransaction);
        __delay_cycles(1000);
        }

        else if(rxBuffer[0] == 0x04||rxBuffer[0]==0x05) // init gereed of actief is
        {
            if(handmatig_profiel == 2) //handmatig geselecteerd
                    {
            txBuffer[0]=0x05; //start register
            txBuffer[1]=0x01; // start
            txBuffer[2]=0x00;
            I2C_transfer(i2cHandle, &i2cTransaction);
            __delay_cycles(1000);

            txBuffer[0]=0x08; //snelheid setpoint register
            txBuffer[1]= aandrijving_snelheid;
            txBuffer[2]= aandrijving_snelheid<<8;
            I2C_transfer(i2cHandle, &i2cTransaction);
            __delay_cycles(1000);}

            else if(handmatig_profiel ==1) //automatisch geselecteerd
            {
                txBuffer[0]=0x05; //start register
                txBuffer[1]=0x01; //start
                txBuffer[2]=0x00;
                I2C_transfer(i2cHandle, &i2cTransaction);
                __delay_cycles(1000);
                if(snelheid_profiel == 1) // constant snelheid
                {
                    txBuffer[0]=0x08; //snelheid setpoint register
                    txBuffer[1]= belasting_max_snelheid;
                    txBuffer[2]= belasting_max_snelheid<<8;
                    I2C_transfer(i2cHandle, &i2cTransaction);
                    __delay_cycles(1000);
                }
                else if(snelheid_profiel == 2) //oplopend snelheid
                {
                    txBuffer[0]=0x08; //snelheid setpoint register
                    txBuffer[1]= oplopende_snelheid;
                    txBuffer[2]= oplopende_snelheid<<8;
                    I2C_transfer(i2cHandle, &i2cTransaction);
                    __delay_cycles(1000);
                    if(oplopende_snelheid<belasting_max_snelheid)//0-14000rpm
                     {
                      oplopende_snelheid=oplopende_snelheid+100; // snelheid loopt als liniair functie omhoog.
                     }
                }
            }
            snelheid_setpoint = txBuffer[2]|txBuffer[1]<<8;
        }
        I2C_close(i2cHandle);
}
void verstuur_belasting(int adres)
{
    //adres belasting = 0x09
    I2C_Params params;
    I2C_Params_init(&params);
    // Open I2C bus for usage
    I2C_Handle i2cHandle = I2C_open(CONFIG_I2C_0, &params);

    I2C_Transaction i2cTransaction;
    i2cTransaction.slaveAddress = adres;
    i2cTransaction.writeBuf   = txBuffer;
    i2cTransaction.writeCount = 3;
    i2cTransaction.readBuf    = rxBuffer;
    i2cTransaction.readCount  = 0;

    I2C_Transaction lezen;
    lezen.slaveAddress = adres;
    lezen.writeBuf   = txBuffer;
    lezen.writeCount = 1;
    lezen.readBuf    = rxBuffer;
    lezen.readCount  = 2;
    txBuffer[0]=0x00; // status belasting
    I2C_transfer(i2cHandle, &lezen);
    status_belasting = rxBuffer[0];

    if(rxBuffer[0] == 0x07) // inititalisatie niet gereed
    {
    txBuffer[0]=0x02; // start register
    txBuffer[1]=0x02; // start init
    txBuffer[2]=0x00;
    I2C_transfer(i2cHandle, &i2cTransaction);
    __delay_cycles(1000);

    txBuffer[0]=0x06; // Maximale temperatuur register
    txBuffer[1]= belasting_temperatuur;
    txBuffer[2]= 0x00;
    I2C_transfer(i2cHandle, &i2cTransaction);
    __delay_cycles(1000);

    txBuffer[0]=0x07; // Maximale snelheid register
    txBuffer[1]=belasting_max_snelheid;
    txBuffer[2]=belasting_max_snelheid<<8;
    I2C_transfer(i2cHandle, &i2cTransaction);
    __delay_cycles(1000);

    txBuffer[0]=0x08; // vermogen setpoint register
    txBuffer[1]= 0x00;
    txBuffer[2]= 0x00;
    I2C_transfer(i2cHandle, &i2cTransaction);
    __delay_cycles(1000);

    }
    else if(rxBuffer[0] == 0x04||rxBuffer[0]==0x06) // Als init is gereed of actief is.
    {
        if(handmatig_profiel == 2) //handmatig geselecteerd
        {
        txBuffer[0]=0x05; //start register
        txBuffer[1]=0x01; //stuur start
        txBuffer[2]=0x00;
        I2C_transfer(i2cHandle, &i2cTransaction);
        __delay_cycles(1000);

        txBuffer[0]=0x09; // vermogen setpoint register
        txBuffer[1]= vermogen_setpoint;
        txBuffer[2]= 0x00;
        I2C_transfer(i2cHandle, &i2cTransaction);
        __delay_cycles(1000);
    }
        else if (handmatig_profiel ==1 ) //automatisch geselecteerd
        {
            txBuffer[0]=0x05; // start register
            txBuffer[1]=0x01; //stuur start
            txBuffer[2]=0x00;
            I2C_transfer(i2cHandle, &i2cTransaction);
            __delay_cycles(1000);

            if(belasting_profiel == 1) // constant vermogen
            {
                txBuffer[0]=0x09; //vermogen setpoint register
                txBuffer[1]= belasting_vermogen; //maximale vermogen
                txBuffer[2]= 0x00;
                I2C_transfer(i2cHandle, &i2cTransaction);
                __delay_cycles(1000);
            }
            else if(belasting_profiel == 2) //oplopend vermogen
            {
                txBuffer[0]=0x09; //vermogen setpoint register
                txBuffer[1]= oplopende_vermogen;
                txBuffer[2]= 0;
                I2C_transfer(i2cHandle, &i2cTransaction);
                __delay_cycles(1000);
                if(oplopende_vermogen<belasting_vermogen)//0-25 Watt
                 {
                  oplopende_vermogen=oplopende_vermogen+1; // snelheid loopt als liniair functie omhoog.
                 }
            }
        }
        vermogen_setpoint = txBuffer[1];

    }
    I2C_close(i2cHandle);
}
void ontvang_aandrijving(int adres)
{
    //adres aandrijving = 0x06
    I2C_Params params;
    I2C_Params_init(&params);
    I2C_Handle i2cHandle = I2C_open(CONFIG_I2C_0, &params);

    I2C_Transaction lezen;
    lezen.slaveAddress = adres;
    lezen.writeBuf   = txBuffer;
    lezen.writeCount = 1;
    lezen.readBuf    = rxBuffer;
    lezen.readCount  = 2;

    txBuffer[0]=0x00; // Status aandrijving register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    status_aandrijving = rxBuffer[0];

    txBuffer[0]=0x01; // Rendement register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    aandrijving_rendement = rxBuffer[0];

    txBuffer[0]=0x02; // Stoom DUT register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    stroom_DUT = rxBuffer[1]|rxBuffer[0]<<8;

    txBuffer[0]=0x03; // Spannig DUT register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    spanning_DUT = rxBuffer[1]|rxBuffer[0]<<8;

    txBuffer[0]=0x04; //snelheid register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    snelheid = rxBuffer[1]|rxBuffer[0]<<8;

    __delay_cycles(1000);
    I2C_close(i2cHandle);
}

void ontvang_belasting(int adres)
{
    I2C_Params params;
    I2C_Params_init(&params);
    // Open I2C bus for usage
    I2C_Handle i2cHandle = I2C_open(CONFIG_I2C_0, &params);
    I2C_Transaction lezen;
    lezen.slaveAddress = adres;
    lezen.writeBuf   = txBuffer;
    lezen.writeCount = 1;
    lezen.readBuf    = rxBuffer;
    lezen.readCount  = 2;

    txBuffer[0]=0x00; //status belasting register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    status_belasting = rxBuffer[0];

    txBuffer[0]=0x02; //rendement register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    belasting_rendement = rxBuffer[0];

    txBuffer[0]=0x03; // temperatuur register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    temperatuur = rxBuffer[0];

    txBuffer[0]=0x04; // koppel register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    koppel = rxBuffer[0];

    txBuffer[0]=0x05; //vermogen register
    I2C_transfer(i2cHandle, &lezen);
    __delay_cycles(1000);
    vermogen = rxBuffer[0];

    __delay_cycles(1000);
    I2C_close(i2cHandle);
}

void Versturen(void){

    /* Call driver init functions */
    I2C_init();
    GPIOinit();
    int Aandrijving= GPIO_read(CONFIG_GPIO_1);
    int Belasting= GPIO_read(CONFIG_GPIO_0);
    if(gestart==1&& Aandrijving==1 && Belasting ==1 ){

        verstuur_aandrijving(0x06);
        verstuur_belasting(0x09);
        ontvang_aandrijving(0x06);
        ontvang_belasting(0x09);
        I2C_Handle i2cHandle;
        I2C_Params params;
        I2C_Params_init(&params);
        i2cHandle = I2C_open(CONFIG_I2C_0, &params);
        if (i2cHandle == NULL) {
        // Error opening I2C
        while (1) {}
        }

        I2C_Transaction i2cTransaction = {0};
        uint8_t readBuffer[2];
        uint8_t writeBuffer[1];

        uint8_t vobj[2];
        writeBuffer[0] = 0x00;
        i2cTransaction.slaveAddress = 0x41;
        i2cTransaction.writeBuf = writeBuffer;
        i2cTransaction.writeCount = 1;
        i2cTransaction.readBuf = readBuffer;
        i2cTransaction.readCount = 2;
        I2C_transfer(i2cHandle, &i2cTransaction);
        vobj[0] = readBuffer[0];
        vobj[1] = readBuffer[1];
        uint16_t vobj_tot = vobj[1] | vobj[0]<<8;
        float vObj;
        if (vobj_tot >= 32768) {
        vObj = ((vobj_tot - 32769.0)*-1.0);
        }
        vObj = vObj * 0.00000015625;


        uint8_t vdie[2];
        writeBuffer[0] = 0x01;
        i2cTransaction.slaveAddress = 0x41;
        i2cTransaction.writeBuf = writeBuffer;
        i2cTransaction.writeCount = 1;
        i2cTransaction.readBuf = readBuffer;
        i2cTransaction.readCount = 2;
        I2C_transfer(i2cHandle, &i2cTransaction);
        vdie[0] = readBuffer[0];
        vdie[1] = readBuffer[1];
        uint16_t vdie_tot = vdie[1] | vdie[0]<<8;
        float tDie = (vdie_tot>>2) / 32.0;
        I2C_close(i2cHandle);

    omgeving_temperatuur1 = tDie;

    int max_vermogen = aandrijving_spanning*aandrijving_stroom;
    int rendement = (belasting_rendement/ aandrijving_rendement)*100;
    int vermogen_aandrijving = stroom_DUT*spanning_DUT;
    int verschil_snelheid = snelheid_setpoint - snelheid;

    sprintf(rendement_buffer, "%d", rendement);
    sprintf(temperatuur_buffer, "%d", temperatuur);
    sprintf(koppel_buffer, "%d", koppel);
    sprintf(snelheid_buffer, "%d", snelheid);
    sprintf(vermogen_b_buffer, "%d", vermogen);
    sprintf(vermogen_a_buffer, "%d", vermogen_aandrijving);
    sprintf(verschil_snelheid_buffer, "%d", verschil_snelheid);
    sprintf(omgeving_temperatuur_buffer, "%d", omgeving_temperatuur1);

    versturen1.belasting = vermogen_b_buffer;
    versturen1.koppel = koppel_buffer;
    versturen1.rendement = rendement_buffer;
    versturen1.snelheid = snelheid_buffer;
    versturen1.temp_belasting = temperatuur_buffer;
    versturen1.vermogen = vermogen_a_buffer;
    versturen1.verschil_snelheid = verschil_snelheid_buffer;
    versturen1.temp_vis = omgeving_temperatuur_buffer;

    if (status_aandrijving == 0x01) //overload Aandrijving
    {
        versturen1.Top = 6;
        gestart=0;
    }
    else if( status_aandrijving == 0x02) // overvoltage
    {
        versturen1.Top = 11;
        gestart=0;
    }
    else if (status_belasting == 0x01 ) // overload Belasting
    {
        versturen1.Top = 10;
        gestart=0;
    }
    else if(status_belasting == 0x02) // overspeed
    {
        versturen1.Top = 5;
        gestart=0;
    }
    else if(status_belasting == 0x03 ) //overheat
    {
        versturen1.Top = 4;
        gestart=0;
    }
    else if(status_belasting == 0x05 && status_aandrijving == 0x04 ) //init gereed
    {
        versturen1.Top = 14;
    }}
    // versturen1.Top = 4 overheat
    // versturen1.Top = 5 overspeed
    // versturen1.Top = 6 overload_A
    // versturen1.Top = 10 overload_B
    // versturen1.Top = 11 overvoltage

}
