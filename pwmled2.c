/*
 * Copyright (c) 2015-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== pwmled2.c ========
 */
/* For usleep() */
#include <unistd.h>
#include <stddef.h>

/* Driver Header files */
#include <ti/drivers/PWM.h>

/* Driver configuration */
#include "ti_drivers_config.h"
#include "mqtt_ontvangen.h"

/*
 *  ======== mainThread ========
 *
 */
void *pwmThread(void *arg0)
{
    /* Period and duty in microseconds */
    uint16_t   pwmPeriod = 100;
//    uint16_t   duty = 0;

    /* Sleep time in microseconds */
    uint32_t   time = 50000;
    PWM_Handle pwm1 = NULL;
    PWM_Params params;

    /* Call driver init functions. */
    PWM_init();

    PWM_Params_init(&params);
    params.dutyUnits = PWM_DUTY_US;
    params.dutyValue = 0;
    params.periodUnits = PWM_PERIOD_US;
    params.periodValue = pwmPeriod;
    pwm1 = PWM_open(CONFIG_PWM_0, &params);
    if (pwm1 == NULL) {
        /* CONFIG_PWM_0 did not open */
        while (1);
    }

    PWM_start(pwm1);

    while (1) {
        PWM_setDuty(pwm1, 0);
        if(omgeving_temperatuur == 0){
            PWM_setDuty(pwm1, 0);
        }
        if(omgeving_temperatuur == 1){
            PWM_setDuty(pwm1, 12);
        }
        if(omgeving_temperatuur == 2){
            PWM_setDuty(pwm1, 19);
        }
        if(omgeving_temperatuur == 3){
            PWM_setDuty(pwm1, 23);
        }
        if(omgeving_temperatuur == 4){
            PWM_setDuty(pwm1, 24);
        }
        if(omgeving_temperatuur == 5){
            PWM_setDuty(pwm1, 32);
        }
        if(omgeving_temperatuur == 6){
            PWM_setDuty(pwm1, 34);
        }
        if(omgeving_temperatuur == 7){
            PWM_setDuty(pwm1, 38);
        }
        if(omgeving_temperatuur == 8){
            PWM_setDuty(pwm1, 41);
        }
        if(omgeving_temperatuur == 9){
            PWM_setDuty(pwm1, 45);
        }
        if(omgeving_temperatuur == 10){
            PWM_setDuty(pwm1, 48);
        }
        if(omgeving_temperatuur == 11){
            PWM_setDuty(pwm1, 52);
        }
        if(omgeving_temperatuur == 12){
            PWM_setDuty(pwm1, 56);
        }
        if(omgeving_temperatuur == 13){
            PWM_setDuty(pwm1, 59);
        }
        if(omgeving_temperatuur == 14){
            PWM_setDuty(pwm1, 63);
        }
        if(omgeving_temperatuur == 15){
            PWM_setDuty(pwm1, 67);
        }
        if(omgeving_temperatuur == 16){
            PWM_setDuty(pwm1, 71);
        }
        if(omgeving_temperatuur == 17){
            PWM_setDuty(pwm1, 73);
        }
        if(omgeving_temperatuur == 18){
            PWM_setDuty(pwm1, 75);
        }
        if(omgeving_temperatuur == 19){
            PWM_setDuty(pwm1, 77);
        }
        if(omgeving_temperatuur == 20){
            PWM_setDuty(pwm1, 79);
        }
        if(omgeving_temperatuur == 21){
            PWM_setDuty(pwm1, 81);
        }
        if(omgeving_temperatuur == 22){
            PWM_setDuty(pwm1, 83);
        }
        if(omgeving_temperatuur == 23){
            PWM_setDuty(pwm1, 84);
        }
        if(omgeving_temperatuur == 24){
            PWM_setDuty(pwm1, 86);
        }
        if(omgeving_temperatuur == 25){
            PWM_setDuty(pwm1, 88);
        }

        usleep(time);
    }
}
